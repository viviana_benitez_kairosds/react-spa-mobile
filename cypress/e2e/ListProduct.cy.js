beforeEach( () => {
	cy.intercept("GET","https://front-test-api.herokuapp.com/api/product",{fixture:"./../fixtures/api.json"});
});


describe("<ListProducts>", () => {

	it("title exist ", () => {
		cy.visit("/");
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(1000);
		cy.get("[data-cy=\"title\"]").should("exist");
		cy.get("#ZmGrkLRPXOTpxsU4jjAcv").should("exist");
	});

	it("filter correctly by brand alcatel", () => {
		cy.visit("/");
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(1000);
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.get("[data-cy=\"input\"]").clear().type("alcatel").wait(1000);
		cy.get("#AasKFs5EGbyAEIKkcHQcF").should("exist");
		cy.get("#ZmGrkLRPXOTpxsU4jjAcv").should("not.exist");
	});
	
	it("filter correctly by model ", () => {
		cy.visit("/");
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(1000);
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.get("[data-cy=\"input\"]").clear().type("plus").wait(1000);
		cy.get("#cGjFJlmqNPIwU59AOcY8H").should("exist");
		cy.get("#ke-gsQbO8qH9PQ-zcdiAJ").should("exist");
		cy.get("#urpRcsTH4SuKPcGrzy-pa").should("exist");
		cy.get("#ZmGrkLRPXOTpxsU4jjAcv").should("not.exist");
	});

	it("click in details of product and redirect to product", () => {
		cy.visit("/");
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(1000);
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.get("[data-cy=\"input\"]").clear().type("alcatel").wait(1000);
		cy.get("#AasKFs5EGbyAEIKkcHQcF").should("exist");
		cy.get("[data-cy=\"detail-AasKFs5EGbyAEIKkcHQcF\"]").click();
		cy.location("pathname").should("include", "details");
		cy.get("[data-cy=\"addCart-AasKFs5EGbyAEIKkcHQcF\"]").should("exist");
	});

});