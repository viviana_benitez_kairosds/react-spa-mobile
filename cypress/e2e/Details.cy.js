beforeEach( () => {
	cy.intercept("GET","https://front-test-api.herokuapp.com/api/product",{fixture:"./../fixtures/api.json"});
});

describe("<Product>", () => {

	it("if click button add cart i should see number cart on image icon cart ", () => {
		cy.visit("/");
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(1000);
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.get("[data-cy=\"input\"]").clear().type("alcatel").wait(1000);
		cy.get("#AasKFs5EGbyAEIKkcHQcF").should("exist");
		cy.get("[data-cy=\"detail-AasKFs5EGbyAEIKkcHQcF\"]").click();
		cy.location("pathname").should("include", "details");
		cy.get("[data-cy=\"addCart-AasKFs5EGbyAEIKkcHQcF\"]").should("exist");
		cy.get("[data-cy=\"storage-2000-AasKFs5EGbyAEIKkcHQcF\"]").click();
		cy.get("[data-cy=\"storage-1000-AasKFs5EGbyAEIKkcHQcF\"]").click();
		cy.get("[data-cy=\"addCart-AasKFs5EGbyAEIKkcHQcF\"]").click();
		cy.get("[data-cy=\"cart_number\"]").should("exist");
	});


	it("if click button add cart without add storage and color should see a message of error ", () => {
		cy.visit("/");
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(1000);
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.get("[data-cy=\"input\"]").clear().type("alcatel").wait(1000);
		cy.get("#AasKFs5EGbyAEIKkcHQcF").should("exist");
		cy.get("[data-cy=\"detail-AasKFs5EGbyAEIKkcHQcF\"]").click();
		cy.location("pathname").should("include", "details");
		cy.get("[data-cy=\"addCart-AasKFs5EGbyAEIKkcHQcF\"]").should("exist");
		cy.get("[data-cy=\"addCart-AasKFs5EGbyAEIKkcHQcF\"]").click();
		cy.get(".sc-dIfARi").should("exist");
		cy.get("[data-cy=\"cart_number\"]").should("not.exist");
	});

	it("if two click button add cart i should see number cart on image icon cart ", () => {
		cy.visit("/");
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(1000);
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.get("[data-cy=\"input\"]").clear().type("alcatel").wait(1000);
		cy.get("#AasKFs5EGbyAEIKkcHQcF").should("exist");
		cy.get("[data-cy=\"detail-AasKFs5EGbyAEIKkcHQcF\"]").click();
		cy.location("pathname").should("include", "details");
		cy.get("[data-cy=\"addCart-AasKFs5EGbyAEIKkcHQcF\"]").should("exist");
		cy.get("[data-cy=\"storage-2000-AasKFs5EGbyAEIKkcHQcF\"]").click();
		cy.get("[data-cy=\"storage-1000-AasKFs5EGbyAEIKkcHQcF\"]").click();
		cy.get("[data-cy=\"addCart-AasKFs5EGbyAEIKkcHQcF\"]").click();
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(500);
		cy.get("[data-cy=\"addCart-AasKFs5EGbyAEIKkcHQcF\"]").click();
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(500);
		cy.get("[data-cy=\"cart_number\"]").should("exist");
		cy.get("[data-cy=\"cart_number\"]").contains("2");
	});


	it("click in Title of header and redirect to product", () => {
		cy.visit("/");
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(1000);
		cy.get("[data-cy=\"detail-ZmGrkLRPXOTpxsU4jjAcv\"]").click();
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(1000);
		cy.location("pathname").should("include", "details");
		cy.get("[data-cy=\"TitleHeader\"]").click();
		cy.location("pathname").should("include", "/");
	});

	


});