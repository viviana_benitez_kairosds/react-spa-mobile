
# react-spa-mobile


## Description

The project is created with Vite and React. And I use javascript and styled components.
for data management the following API is used: https://front-test-api.herokuapp.com/api/product

This application consists of the creation of a mini-application to buy mobile devices.

The application will have only two views:
1. Main view - List of products
2. Product details


## Getting started

Installation

Clone the repo either with SSH or HTTPS:
```
git clone https://gitlab.com/viviana_benitez_kairosds/react-spa-mobile.git
```

or
```
git clone git@gitlab.com:viviana_benitez_kairosds/react-spa-mobile.git
```

Install NPM packages
```
npm install
```

Start the project
```
npm run start
```

Run the production build
```
npm run build
```

Run the tests

```
npm run test
```

Run the end to end tests
```
npm run cy
```

Run the linter

```
npm run lint
```


## Test and Deploy

Use the built-in continuous integration in GitLab.

I have done cypress test to check the flow of the application and vitest with testing library for custom hook and use case.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Deploy to Docker](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)

***


##Image 
![Semantic description of image](src/assets/mobilePage.png "")

![Semantic description of image](src/assets/detailMobile.png "")

