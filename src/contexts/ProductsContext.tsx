import React, { createContext,useState } from "react";

export const CartContext = createContext();

const defaultSettings =  0;

export const ProductsContext = ({ children }) => {
  const [currentSettings, setCurrentSettings] = useState(defaultSettings);

  // const saveSettings = (values) => {
  //   setCurrentSettings(values)
  // };

  return (
    <CartContext.Provider
      value={{ currentSettings, setCurrentSettings }}
    >
      {children}
    </CartContext.Provider>
  );
};

export default CartContext;
