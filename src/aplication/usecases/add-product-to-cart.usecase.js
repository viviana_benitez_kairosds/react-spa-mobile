

export class addProductToCartUseCase{

	service;

	constructor(service) {
		this.service = service;
	}

	async execute(id,storageCode,colorCode){
		return await this.service.addProduct(id,storageCode,colorCode);
	}
}