
export class getListProductsUseCase{
	service;

	constructor(service) {
		this.service = service;
	}

	async execute(){
		return await this.service.getListProducts();
	}
}