

export class getProductUseCase{

	service;

	constructor(service) {
		this.service = service;
	}

	async execute(id){
		return await this.service.getProduct(id);
	}
}