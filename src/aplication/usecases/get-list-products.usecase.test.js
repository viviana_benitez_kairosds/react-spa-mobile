import { describe, expect, it, vi } from "vitest";
import { ListProductsService } from "../../domain/services/list-products.service";
import { getListProductsUseCase } from "./get-list-products.usecase";
import PRODUCTS from "../../fixtures/api.json";

console.log("getListProducts ", );

describe("Get list products use case", () => {

	it("should be called once", async () => {
		const service = new ListProductsService();
		const getAllProducts = vi.spyOn(service, "getListProducts");
		const usecase = new getListProductsUseCase(service);
		usecase.execute();
		expect(getAllProducts).toHaveBeenCalled();
		expect(getAllProducts).toHaveBeenCalledTimes(1);
	});

	it("should be return all list product", async () => {
		const service = new ListProductsService();
		const getAllProducts = vi.spyOn(service, "getListProducts");
		const usecase = new getListProductsUseCase(service);
		usecase.execute();
		getAllProducts.mockResolvedValue(PRODUCTS);
	});
});
