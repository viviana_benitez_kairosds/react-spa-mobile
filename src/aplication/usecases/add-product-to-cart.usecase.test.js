import { describe, expect, it, vi } from "vitest";
import { ProductService } from "../../domain/services/product.service";
import { addProductToCartUseCase } from "./add-product-to-cart.usecase";

describe("Add Product To Cart UseCase", () => {
	it("should be called once", async () => {
		const id=1; 
		const storageCode = 2000;
		const colorCode = 1001;
		const service = new ProductService();
		const getProductSpy = vi.spyOn(service, "addProduct");
		const usecase = new addProductToCartUseCase(service);
		usecase.execute(id, storageCode, colorCode);
		expect(getProductSpy).toHaveBeenCalled();
		expect(getProductSpy).toHaveBeenCalledTimes(1);
		expect(getProductSpy).toHaveBeenCalledWith(id, storageCode, colorCode);
	});
});
