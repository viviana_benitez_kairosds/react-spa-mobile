import { describe, expect, it, vi } from "vitest";
import { ProductService } from "../../domain/services/product.service";
import { getProductUseCase } from "./get-product.usecase";


describe("Get product use case", () => {

	it("should be called once", async () => {

		const id = 1;

		const service = new ProductService();
		const getProductSpy = vi.spyOn(service, "getProduct");
		const usecase = new getProductUseCase(service);
		usecase.execute(id);
		expect(getProductSpy).toHaveBeenCalled();
		expect(getProductSpy).toHaveBeenCalledTimes(1);
		expect(getProductSpy).toHaveBeenCalledWith(1);

	});
});
