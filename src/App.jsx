import {BrowserRouter, Routes, Route } from "react-router-dom";
import ListProduct from "./domain/components/List-Product";
import Product from "./domain/components/Product";
import "./App.css";
import Header from "./domain/components/Header";
import {ProductsContext} from "./contexts/ProductsContext";

function App() {

	// const counter = useContext(CartContext);
	return (
		<BrowserRouter>
			<ProductsContext>
				<Header/>
				<Routes>
					<Route path='/' element={<ListProduct />}/>
					<Route path='/listProducts' element={<ListProduct />}/>
					<Route path='/details' element={<Product/>}/>
					<Route path='*' element={<h1>Página no encontrada</h1> }/>
				</Routes>
			</ProductsContext>
		</BrowserRouter>
	);
}

export default App;
