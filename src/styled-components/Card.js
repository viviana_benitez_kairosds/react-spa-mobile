import styled from "styled-components";
import { Link } from "react-router-dom";

export const Wrapper = styled.div`
  margin: 20px;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`;

export const CardContent= styled.div`
    width: 23%;
    height: 420px;
    margin: 15px;
    box-sizing: border-box;
    text-align: center;
    border-radius: 10px;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
    padding-top: 10px;
    box-shadow: 0 0 10px rgb(0 0 0 / 10%);
    transition: .4s;

    /* @media screen and (max-width: 412px){
      width: 40%;
    }

    @media screen and (max-width: 360px){
      width: 50%;
    } */

    @media screen and (max-width: 700px) {
        width: 100%;
        display: block;
        margin-bottom: 20px;
    }

`;

export const CardImage = styled.div`
  background-image: url(${({background} ) => background});
  /* min-width: 160px;
  min-height: 212px; */
  width: 160px;
  height: 210px;
  text-align: center;
  margin: 0 auto;
  display: block;

  @media screen and (max-width: 800px){
    width: 160px;
    height: 210px;
    object-fit: cover;
    }
`;

export const Title = styled.h3`
  text-align: center;
  font-size: 30px;
  margin: 0;
  padding-top: 10px;
`;
export const Brand = styled.div`
  grid-area: text;
  margin: 25px;
`;

export const Model = styled.h2`
  color: grey;
`;

export const Price = styled.h6`
  font-size: 26px;
  text-align: center;
  color: #222f3e;
  margin: 10px;
`;

export const NavLink = styled(Link)`
  font-family: "Arial Black", Gadget, sans-serif;
  margin-top: 5px;
  padding: 11px 13px;
  color: rgb(253, 249, 243);
  font-weight: 600;
  text-transform: uppercase;
  border: none;
  border-radius: 5px;
  width: 100%;
  cursor: pointer;
  transition: 0.5s;
  background-size: 200% auto;
  outline: none;
  background-image: linear-gradient(to right, #84fab0 0%, #8fd3f4 51%, #84fab0 100%);
  
  &:hover{
    background-position: right center;
  }
`;

export const Image = styled.h6`
  font-size: 26px;
  text-align: center;
  color: #222f3e;
  margin: 0;
`;