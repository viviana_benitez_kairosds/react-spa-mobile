import styled from "styled-components";

export const Storage = styled.button`
  max-width: 50px;
  border: 1px solid black;
  cursor: pointer;
  padding: 2px;
  margin: 5px;

  &:hover{
    /* border: 1px solid blue; */
    background-color: #ECEAEA;
  }

`;


export const Color = styled.button`
  width: 20px;
  height: 20px;
  border-radius: 100%;
  cursor: pointer;
  border: 1px solid black;
  background-color: ${ props =>  props.color ? props.color : ""};
  padding: 2px;
  margin: 5px;

  &:hover{
    border: 1px solid blue;
  }
`;