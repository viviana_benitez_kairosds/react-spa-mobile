import styled from "styled-components";

export const WrapperSection = styled.section`
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 5%;

`;

export const ImgProduct = styled.img`
    width: 30vh;
    height: 40vh;
`;

export const ListDetail = styled.li`
    display: flex;
    justify-content: flex-start;
    justify-items: center;
    align-items: center;
    list-style-type: none;
    font-family: "Arial Black", Gadget, sans-serif;
`;