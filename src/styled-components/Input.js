import styled from "styled-components";

export const Input = styled.input`
    height: 2vh;
    border-radius: 3px;
    padding: 0.5em;
    margin: 0.5em;
    font-size: 24px;
`;

export const WrapperInput = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-between;
    align-items: center;
    justify-items: center;

    @media screen and (max-width: 700px) {
        flex-direction: column;
    }
`;

