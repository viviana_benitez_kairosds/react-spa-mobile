import styled from "styled-components";
import { Link } from "react-router-dom";

export const Nav = styled.header`
    height: 8vh;
    width: 100%;
    display: flex;
    justify-content: space-around;
    align-items: center;
    justify-items: center;
    color: whitesmoke;
    font-size: 2.2rem;
    background-image: linear-gradient(to right, #84fab0 0%, #8fd3f4 51%, #84fab0 100%);
`;


export const LinkCart = styled(Link)`
  font-family: "Arial Black", Gadget, sans-serif;
  text-decoration: none;
  color: #fff;

`;


export const Button = styled.button`
    font-family: "Arial Black", Gadget, sans-serif;
    margin-top: 5px;
    padding: 11px 13px;
    color: rgb(253, 249, 243);
    font-weight: 600;
    text-transform: uppercase;
    border: none;
    border-radius: 5px;
    width: 50%;
    cursor: pointer;
    transition: 0.5s;
    background-size: 200% auto;
    outline: none;
    background-image: linear-gradient(to right, #84fab0 0%, #8fd3f4 51%, #84fab0 100%);
    
    &:hover{
      background-position: right center;
    }
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;

`;
