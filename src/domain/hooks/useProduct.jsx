import { useEffect, useState } from "react";
import { getProductUseCase } from "../../aplication/usecases/get-product.usecase";
import {ProductService} from "../../domain/services/product.service";

const useProduct = (id) => {
	const [product,setProduct] = useState({});
	const service = new ProductService();
	const usecases = new getProductUseCase(service);
	useEffect( () => {
		validateSession(id);
	},[]);


	// Get from the session and validate for each reload or any event.
	function validateSession(id) {
		let cache = localStorage.getItem(id);
		if (cache !== null) {
			let sessionObj = JSON.parse(cache);
			if (sessionObj.expiredAt > Date.now()) { // Validate expiry date.
				setProduct(sessionObj.value);
			} else {
				localStorage.removeItem(id);
			}
		}else{
			usecases.execute(id).then(
				(product) => {
					setProduct(product);
					setLocalStorage(product.id, product);
				});
		}
	}
     
	// // Set session function
	function setLocalStorage(id, product) {
		let obj = {
			value: product,
			expiredAt:  new Date().getTime() + 3600000
		};
		localStorage.setItem(id, JSON.stringify(obj));
	}


	return {product};
};

export default useProduct;