import { useEffect, useState } from "react";
import { getListProductsUseCase } from "../../aplication/usecases/get-list-products.usecase";
import { ListProductsService } from "../services/list-products.service";

const useListProducts = () => {

	const [products,setProducts] = useState([]);
	const [productsFilters, setProductsFilters] = useState(products);
	const service = new ListProductsService();
	const usecases = new getListProductsUseCase(service);
	
	useEffect( () => {
		validateSession();
	},[]);

	

	// Get from the session and validate for each reload or any event.
	function validateSession() {
		let cache = localStorage.getItem("listProducts");
		if (cache !== null) {
			let sessionObj = JSON.parse(cache);
			if (sessionObj.expiredAt > Date.now()) { // Validate expiry date.
				setProducts(sessionObj.value);
				setProductsFilters(sessionObj.value);
			} else {
				localStorage.removeItem("listProducts");
			}
		}else{
			usecases.execute().then(
				(productList) => {
					setProducts(productList);
					setProductsFilters(productList);
					setLocalStorage(productList);
				});
		}
	}
  
	// Set session function
	function setLocalStorage(value) {
		let obj = {
			value,
			expiredAt:  new Date().getTime() + 3600000
		};
		localStorage.setItem("listProducts", JSON.stringify(obj));
	}


	const filter = (word) => {
		var result = productsFilters.filter((product) => {
			return product.brand.toLowerCase().includes(word.toLowerCase()) || product.model.toLowerCase().includes(word.toLowerCase());
		});
		setProducts(result);
	};

    
	return {products,filter};
};

export default useListProducts;