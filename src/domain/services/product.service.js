import { ProxyRepository } from "../../infrastructure/repositories/proxy-repository";

export class ProductService{

	async getProduct(id){
		const proxyRepository = new ProxyRepository();
		return proxyRepository.getProduct(id);
	}

	async addProduct(id,storageCode, colorCode){
		const proxyRepository = new ProxyRepository();
		return await proxyRepository.addProduct(id,storageCode, colorCode);
	}

}