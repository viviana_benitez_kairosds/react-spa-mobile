import { ProxyRepository } from "../../infrastructure/repositories/proxy-repository";

export class ListProductsService{

	async getListProducts(){
		const proxyRepository = new ProxyRepository();
		return proxyRepository.getListProducts();
	}

}