import React from "react";
import {CardContent}from "../../styled-components/Card";
import {CardImage}  from "../../styled-components/Card";
import {Brand}  from "../../styled-components/Card";
import {Model} from "../../styled-components/Card";
import {Price}  from "../../styled-components/Card";
import {NavLink} from "../../styled-components/Card";


const Card = ({product}) => (
	<>
		<CardContent id={product.id}>
			<CardImage background={product.imgUrl} />
			<Brand>{product.brand}</Brand>
			<Model>{product.model}</Model>
			{
				product.price ? <Price>{product.price}€</Price> : <Price>Precio no disponible</Price>
			}
			
			<NavLink 
				data-cy={`detail-${product.id}`}
				to={{
					pathname: "/details",
					search: `id=${product.id}`
				
				}}>Detalles</NavLink>
			
		</CardContent>
	</>
);
export default Card;