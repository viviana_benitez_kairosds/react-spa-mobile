import { useState } from "react";
import Card from "./Card";
import { Wrapper } from "../../styled-components/Card";
import { Input } from "../../styled-components/Input";
import { WrapperInput } from "../../styled-components/Input";
import useListProducts from "../hooks/useListProducts";


function ListProduct() {
	const {products,filter} = useListProducts();
	const [search, setSearch] = useState("");
	// console.log("products ", products);
	const handleChange = e => {
		setSearch(e.target.value);
		filter(e.target.value);
	};


	return ( 
		<>
			<Wrapper>
				<WrapperInput>
					<h1 data-cy="title">Lista productos</h1>
					<Input
						data-cy="input"
						value={search}
						placeholder="buscar..."
						onChange={handleChange}
					/>
				</WrapperInput>
				{products.length>0 ? products.map((product) => (
					<Card key={product.id} product={product}></Card>
				)) : <h1>No se han encontrado móviles con ese filtro</h1>
				}
			</Wrapper>
		</> 
	);
}

export default ListProduct;
