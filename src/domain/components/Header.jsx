import React, { useContext }  from "react";
import {Nav} from "../../styled-components/Global";
import {LinkCart} from "../../styled-components/Global";
import "bootstrap-icons/font/bootstrap-icons.css";
import { CartContext } from "../../contexts/ProductsContext";

const Header = () => {
	const {currentSettings} = useContext(CartContext);
	return (
		<>
			<Nav>
				<LinkCart 
					data-cy="TitleHeader"
					to={{
						pathname: "/",
					}}>·Mobile·
				</LinkCart>

				<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" rel="stylesheet"/>
				<div className="cart-menu align-items-center d-flex">
					<div className="sidebar-social">
						<p  className="cart"><i data-cy="cart-icon" className="fas fa-shopping-cart"></i>
							{
								currentSettings > 0 ?
									<span data-cy="cart_number" id="cart_menu_num" data-action="cart-can" className="badge rounded-circle">{currentSettings}</span>
									: ""
							}
						</p>
					</div>
				</div>
			</Nav>
		</>
	);};

export default Header;