import { useContext, useState} from "react";
import { useSearchParams } from "react-router-dom";
import { WrapperSection } from "../../styled-components/Detail";
import { ImgProduct } from "../../styled-components/Detail";
import { ListDetail } from "../../styled-components/Detail";
import { Button, Container } from "../../styled-components/Global";
import { Color, Storage } from "../../styled-components/selectors";
import { Breadcrumbs, Crumb } from "../../styled-components/breadcrumbs";
import { Link,useLocation } from "react-router-dom";
import { Error } from "../../styled-components/Errors";
import useProduct from "../hooks/useProduct";
import { CartContext } from "../../contexts/ProductsContext";
import {addProductToCartUseCase} from "../../aplication/usecases/add-product-to-cart.usecase";
import { ProductService } from "../../domain/services/product.service";

function Product() {
	const location = useLocation();
	const [searchParams, setSearchParams] = useSearchParams();
	const id = searchParams.get("id");
	
	const {product} = useProduct(id);
	const colorDefault = product.options?.colors.length == 1 ? product.options?.colors[0].code : "";
	const storageDefault = product.options?.storages.length == 1 ? product.options?.storages[0].code : "";
	const [optionSelected, setOptionSelected] = useState({id:"",color: colorDefault, storage: storageDefault});
	const [error,setError] = useState("");
	const {currentSettings,setCurrentSettings} = useContext(CartContext);
	
	const addCart = async (idProduct) =>  {
		setOptionSelected({...optionSelected,id: idProduct});
		
		if(!!optionSelected.storage && !!optionSelected.color){
			setError("");
			const service = new ProductService();
			const usecase = new addProductToCartUseCase(service);
			const contadorResult = await usecase.execute(id, optionSelected.storage, optionSelected.color);
			setCurrentSettings(currentSettings+contadorResult.count);
		}else{
			setError("Debe tener las opciones seleccionadas");
		}
		
	};

	return (
		<>
			<div>
				<Breadcrumbs>
					<Crumb>
						<Link to="/listProducts"
							className={location.pathname === "/listProducts" ? "breadcrumb-active" : "breadcrumb-not-active"}
						>List Products</Link>
					</Crumb>
					<Crumb>
						<Link to={{
							pathname: "/details",
							search: `id=${product.id}`
				
						}}>{product.model}</Link>
					</Crumb>
				</Breadcrumbs>
			</div>
			<WrapperSection>
				<div>
					<ImgProduct src={product.imgUrl} />
				</div>
				<Container>
					<ul>
						<h1> {product.model}</h1>
						<h1>{product.price} €</h1>
						<ListDetail><b>Marca:</b> {product.brand}</ListDetail>
						<ListDetail><b>Modelo:</b> {product.model}</ListDetail>
						<ListDetail><b>CPU:</b> {product.cpu}</ListDetail>
						<ListDetail><b>RAM:</b> {product.ram}</ListDetail>
						<ListDetail><b>Sistema Operativo:</b> {product.os}</ListDetail>
						<ListDetail><b>Resolución:</b> {product.displayResolution}</ListDetail>
						<ListDetail><b>Batería:</b> {product.battery}</ListDetail>
						<ListDetail><b>Cámaras:</b> {product.displaySize}</ListDetail>
						<ListDetail><b>Dimensiones:</b> {product.dimentions}</ListDetail>
						<ListDetail><b>Peso:</b> {product.weight} g</ListDetail>
						<ListDetail><b>Almacenamiento:</b> 
							{ product && product.options?.storages.map((storage) => 
								<Storage onClick={() => setOptionSelected({...optionSelected,storage: storage.code})} key={storage.code} data-cy={`storage-${storage.code}-${product.id}`}>{storage.name}</Storage>
							)}

						</ListDetail>
						<ListDetail><b>Color: </b>
							{ product && product.options?.colors.map((color) => (<Color onClick={() => setOptionSelected({...optionSelected,color: color.code})}  key={color.code} color={color.name} data-cy={`storage-${color.code}-${product.id}`}></Color>))}
						</ListDetail>
						<Button data-cy={`addCart-${product.id}`} onClick={() => addCart(product.id)}>Añadir a la cesta</Button><br></br>
						<Error>{error}</Error>
					</ul>
				</Container>
			</WrapperSection>
		</>
	);
}

export default Product;
