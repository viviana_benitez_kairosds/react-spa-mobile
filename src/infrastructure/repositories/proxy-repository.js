import axios from "axios";

export class ProxyRepository{
    
	async getListProducts(){
		const url = "https://front-test-api.herokuapp.com/api/product";
		const {data} = await axios(url);
		return data;
	}


	async getProduct(id){
		const url = `https://front-test-api.herokuapp.com/api/product/${id}`;
		const {data} = await axios(url);
		return data;
	}

	async addProduct(id,storageCode, colorCode){
		
		const {data} = await axios.post("https://front-test-api.herokuapp.com/api/cart", {
			id: id,
			colorCode: colorCode,
			storageCode: storageCode
		});
		return data;
	}
}