FROM node:16-alpine3.14 as build
RUN mkdir /react-app
WORKDIR /react-app
COPY . .
RUN npm ci
RUN npm run build
FROM nginx:1.23.1-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build /react-app/dist /usr/share/nginx/html/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]